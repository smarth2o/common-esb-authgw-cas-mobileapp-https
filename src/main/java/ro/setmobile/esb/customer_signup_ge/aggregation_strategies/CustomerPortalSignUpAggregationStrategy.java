package ro.setmobile.esb.customer_signup_ge.aggregation_strategies;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;


import org.codehaus.jackson.map.ObjectMapper;

import com.sun.istack.logging.Logger;

public class CustomerPortalSignUpAggregationStrategy implements AggregationStrategy {
	
	private static Logger log=Logger.getLogger(CustomerPortalSignUpAggregationStrategy.class);
	
	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		if (newExchange == null) {
			return oldExchange;
		}
		
		String jsonOld=oldExchange.getIn().getBody().toString();
		String jsonNew=newExchange.getIn().getBody().toString();
		Map<String,Object> jsonOldMapper;
		Map<String,Object> jsonNewMapper;
		Map<String,Object> jsonSendToGE=new HashMap<String, Object>();
		
		ObjectMapper mapper = new ObjectMapper();
		try{
			jsonOldMapper = mapper.readValue(jsonOld, Map.class);
			jsonNewMapper = mapper.readValue(jsonNew, Map.class);
		}catch(Exception e){
			return newExchange;
		}
		
		newExchange.getIn().setHeader("stopForwardingRequestToGE", 2);
		
		String error_code=null;
		if(jsonNewMapper.get("error_code")!=null)
			error_code = jsonNewMapper.get("error_code").toString();
		
		log.info("****** mobilews error_code - " + error_code);
		if(error_code.equals("error_code_0")) {

			String value=null;
			if(jsonOldMapper.get("group_id")!=null)	{	
				value = jsonOldMapper.get("group_id").toString();	
							
				try{
					Integer groupId = Integer.parseInt(value);
					if( groupId == 2 || groupId == 3 ){
						newExchange.getIn().setHeader("forwardingRequestToGE", groupId);
					}
					
					String userId=jsonNewMapper.get("oid").toString();
					if(userId!=null){
											
						try{
							Integer oid = Integer.parseInt(userId);					
							
							jsonSendToGE.put("username",jsonOldMapper.get("username"));
							jsonSendToGE.put("email",jsonOldMapper.get("email"));
							jsonSendToGE.put("zipcode",jsonOldMapper.get("zipcode"));
							jsonSendToGE.put("country",jsonOldMapper.get("country"));
							jsonSendToGE.put("password",jsonOldMapper.get("password"));
							jsonSendToGE.put("group", groupId);
							jsonSendToGE.put("global_id", oid);
							
							log.info("username - " + jsonOldMapper.get("username"));
							log.info("email - " + jsonOldMapper.get("email"));
							log.info("zipcode - " + jsonOldMapper.get("zipcode"));
							log.info("country - " + jsonOldMapper.get("country"));
							log.info("password - " + jsonOldMapper.get("password"));
							log.info("group - " + groupId);
							log.info("global_id - " + oid);
											
							String result = mapper.writeValueAsString(jsonSendToGE);
							log.info("RESULT JSON "+result);
							oldExchange.getIn().setBody(result);
							return oldExchange;
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		
			newExchange.getIn().setHeader("forwardingRequestToGE", new Integer(2));
			
		}
		
		return newExchange;
		
	}

}
