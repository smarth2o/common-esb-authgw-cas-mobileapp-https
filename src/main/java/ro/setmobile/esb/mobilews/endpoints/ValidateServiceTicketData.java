/**
 * 
 */
package ro.setmobile.esb.mobilews.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * @author dan
 *
 */
@Path("/validateServiceTicketData")
public class ValidateServiceTicketData {

	@POST
	@Path("/validateServiceTicket")
	@Produces(MediaType.TEXT_PLAIN)
	public String validateServiceTicket(@QueryParam("ticket") String ticket,@QueryParam("service") String service){
		return null; 
	}
}
