/**
 * 
 */
package ro.setmobile.esb.mobilews.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author dan
 *
 */
@Path("/logoutData")
public class LogoutData {

	@DELETE
	@Path("/logout/{tgt}")
	@Produces(MediaType.TEXT_PLAIN)
	public String  logout(@PathParam("tgt") String tgt) {
		return null; 
	}
}
