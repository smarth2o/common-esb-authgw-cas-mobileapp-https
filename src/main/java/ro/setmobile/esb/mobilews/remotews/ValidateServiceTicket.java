package ro.setmobile.esb.mobilews.remotews;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
public interface ValidateServiceTicket {

	@POST	
	@Produces(MediaType.TEXT_PLAIN)
	public String validateServiceTicket(@QueryParam("ticket") String ticket,@QueryParam("service") String service);

}