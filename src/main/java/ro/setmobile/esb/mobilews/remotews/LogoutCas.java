package ro.setmobile.esb.mobilews.remotews;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/{tgt}")
public interface LogoutCas {

	@DELETE	
	@Produces(MediaType.TEXT_PLAIN)
	public String logout(@PathParam("tgt") String tgt);

}